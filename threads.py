# multiple_sync_request_threaded.py
import threading  
import requests
import time

def make_requests(session, n, url):  
    for i in range(n):
        resp = session.get(url)
        if resp.status_code == 200:
            pass

def main():  
    n_threads = 20
    n_requests = 1000
    n_requests_per_thread = n_requests // n_threads

    url = "http://192.168.23.4:88"
    session = requests.Session()

    threads = [
        threading.Thread(
            target=make_requests,
            args=(session, n_requests_per_thread, url)
        ) for i in range(n_threads)
    ]

    start = time.time()
    for t in threads:
        t.start()
    for t in threads:
        t.join()
    end = time.time()
    total = end-start
    print("took %02.02f seconds" % (total))
    print("%02.02f requests x second" % (n_requests*1.0/total))
    print("average requests time %02.04f s" % (total*1.0/n_requests))

    print("|python|threads| %02.02f s | %02.02f | %02.02f | %02.04f s| (units in seconds here)" % (
        total,
        n_requests*1.0/total,
        n_threads*total*1.0/n_requests,
        total*1.0/n_requests
    ))

main()  