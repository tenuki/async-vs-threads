# multiple_async_requests.py
import asyncio  
import aiohttp
import time

async def make_request(session, qty_req, req_n):  
    url = "http://192.168.23.4:88"
    for idx in range(qty_req):
        async with session.get(url) as resp:
            if resp.status == 200:
                await resp.text()


async def main():  
    n_threads = 20
    n_requests = 1000
    start = time.time()
    async with aiohttp.ClientSession() as session:
        await asyncio.gather(
            *[make_request(session, int(n_requests/n_threads), i) for i in range(n_threads)]
        )
    end = time.time()
    total = end-start
    print("took %02.02f seconds" % (total))
    print("%02.02f requests x second" % (n_requests*1.0/total))
    print("average requests time %02.04f s" % (total*1.0/n_requests))
    print("|python|async2| %02.02f s | %02.02f |  %02.04f s| %02.04f s| (units in seconds here)" % (
        total,
        n_requests*1.0/total,
        n_threads*total*1.0/n_requests,
        total*1.0/n_requests
    ))

loop = asyncio.get_event_loop()  
loop.run_until_complete(main())  
